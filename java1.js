				function validateForm() 
				{
				    var checkfname = document.forms["myForm"]["fname"].value;
				    var checksurname = document.forms["myForm"]["surname"].value;
				    var checkuser = document.forms["myForm"]["username"].value;
				    var checkpass = document.forms["myForm"]["password"].value;
				    var checkrepass = document.forms["myForm"]["repassword"].value;
				    var checkfalse = /^[0-9a-zA-Z]+$/;
				    var checkage = document.forms["myForm"]["age"].value;
				    if (checkfname == null || checkfname == "") 
				    {
				        alert("Name must be filled out");
				        return false;
				    }
				    if (checkfname.length<3)
				    {
				    	alert("Forename must have at least 3 alphabet characters");
				        return false;
				    }
				    if (checksurname != checkfname)
				    {
				    	alert("Surname not match with Forename")
				    	return false;
				    }
				    if (checkuser.length <5)
				    {
				    	alert("Username must be at least 5 characters.")
				    	return false;
				    }
				    if (!/^[a-zA-Z0-9_-]*$/g.test(document.myForm.username.value)) {
				        alert("Invalid characters");
				        document.myForm.username.focus();
				        return false;
				    }
				    if (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/g.test(document.myForm.password.value))
				    {
				    	alert("Password must be at least 8 characters, containing both upper and lower case letters, numbers and symbols.")
				    	document.myForm.password.focus();
				    	return false;
				    }
				    if (checkrepass != checkpass)
				    {
				    	alert("Re-Password must be same the Password")
				    	return false;
				    }

				    if (checkage <18|| checkage>110)
				    {
				    	alert("It is a 18+ website, so the age must be between 18 and 110")
				    	return false;
				    }
				    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/g.test(document.myForm.email.value))  
					{  
						alert("must be of the form abc@def.ghi")
					    return false;  
					}  
				}
